
#include "../inc/Cure.hpp"

//Constructors and destructor
Cure::Cure (void)
	: AMateria("cure")
{

}

Cure::Cure (Cure const &cure)
	: AMateria(cure._type)
{}

Cure::~Cure ()
{
	std::cout<<"Calling Cure destructor"<<std::endl;
}

//Operators

const Cure& Cure::operator= (Cure const &cure)
{
    AMateria::operator= (cure);
	return *this;
}

//Virtual methods inherited

AMateria* Cure::clone () const
{
	Cure *cure = new Cure();

	return cure;
}

void Cure::use (ICharacter &target)
{
	std::cout<<"* heals "<<target.getName()<<"´s wounds *"<<std::endl;
}
