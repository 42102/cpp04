
#include "../inc/MateriaSource.hpp"

//Constructors and destructor

MateriaSource::MateriaSource (void)
{
    for(unsigned int i = 0; i < NUM_MATERIAS; i++)
    {
        this->_materias[i] = NULL;
    }
}

MateriaSource::MateriaSource (MateriaSource const &materia_src)
{
    *this = materia_src;
}
MateriaSource::~MateriaSource ()
{
    std::cout<<"Calling MateriaSource destructor"<<std::endl;

    for(unsigned int i = 0; i < NUM_MATERIAS; i++)
    {
        if(this->_materias[i] != NULL)
            delete (this->_materias[i]);
    }
}

//Operators

const MateriaSource& MateriaSource::operator= (MateriaSource const &materia_src)
{
    unsigned int pos = 0;

    while(pos < NUM_MATERIAS)
    {
        if(this->_materias[pos] != NULL)
            delete(this->_materias[pos]);
        pos++;
    }

    for(pos = 0; pos < NUM_MATERIAS; pos++)
    {
        if(materia_src._materias[pos]->getType() == "ice")
        {
            this->_materias[pos] = new Ice();
            *(this->_materias[pos]) = *(materia_src._materias[pos]);
        }
        else if(materia_src._materias[pos]->getType() == "cure")
        {
            this->_materias[pos] = new Cure();
            *(this->_materias[pos]) = *(materia_src._materias[pos]);
        }
        else
        {
            std::cout<<"There is a materia which doesnt exist"<<std::endl;
        }
    }

	return *this;
}

//Virtual inherited methods

//En este metodo comprobar primero que haya hueco y luego insertar la materia
void MateriaSource::learnMateria(AMateria* materia)
{
   unsigned int pos = 0;

  while(pos < NUM_MATERIAS && this->_materias[pos] != NULL)
	  pos++;

	if(pos >= NUM_MATERIAS)
	{
		std::cout<<"All slots are occupied"<<std::endl;
	}
	else if(typeid(*(materia)).name() == typeid(Ice).name())
	{
		this->_materias[pos] = new Ice();
		*(this->_materias[pos]) = *materia;
	}
	else if(typeid(*(materia)).name() == typeid(Cure).name())
	{
		this->_materias[pos] = new Cure();
		*(this->_materias[pos]) = *materia;
	}
	else
	{
		std::cout<<"The specified materia not exists"<<std::endl;
	}
}

//En este metodo controlar que el tipo exista y se corresponda con alguna
AMateria* MateriaSource::createMateria(std::string const & type)
{
	unsigned int pos = 0;
	
	while(pos < NUM_MATERIAS)
	{
		if(this->_materias[pos] != NULL)
		{
			if (this->_materias[pos]->getType().compare(type) == 0)
				break;
		}
		pos++;
	}

	if(pos >= NUM_MATERIAS)
	{
		std::cout<<"Type of materia specified non existing"<<std::endl;
    	return NULL;
	}
	else
	{
		return this->_materias[pos];		
	}
}
