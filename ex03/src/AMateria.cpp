
#include "../inc/AMateria.hpp"

//Constructors and destructor

AMateria::AMateria (void)
    : _type("none type")
{
    std::cout<<"AMateria default constructor"<<std::endl;
}

AMateria::AMateria (AMateria const &materia)
    : _type(materia._type)
{
    std::cout<<"AMateria copy constructor"<<std::endl;
}

AMateria::AMateria (std::string const &type)
    : _type(type)
{
    std::cout<<"AMateria constructor with 1 parameter<string>"<<std::endl;
}

AMateria::~AMateria()
{
    std::cout<<"Calling AMateria destructor"<<std::endl;
}

//Operators

const AMateria& AMateria::operator= (AMateria const &materia)
{
    if(this != &materia)
        this->_type = materia._type;

    return *this;
}

//Methods

std::string const& AMateria::getType () const
{
	return (this->_type);
}

void AMateria::use (ICharacter& target)
{
    (void)target;
}

