
#include "../inc/Character.hpp"

//Constructors and destructor

Character::Character (void)
	: _name("undefined_character")
{
	delete_list = NULL;
	for(unsigned int i = 0; i < NUM_OF_SLOTS; i++)
	{
		_inventory[i] = NULL;
	}
}

Character::Character (std::string name)
	: _name(name)
{	
	
	delete_list = NULL;
	for(unsigned int i = 0; i < NUM_OF_SLOTS; i++)
	{
		_inventory[i] = NULL;
	}
}

Character::Character (Character const &c)
{
	
	delete_list = NULL;
	copyDeleteList(c.delete_list);
	for(unsigned int i = 0; i < NUM_OF_SLOTS; i++)
	{
		_inventory[i] = NULL;
	}
	*this = c;
}

Character::~Character()
{
	if(delete_list != NULL)
	{
		removeDeleteList();
	}
	std::cout<<"Calling Character destructor"<<std::endl;
    for(unsigned int i = 0; i < NUM_OF_SLOTS; i++)
    {
        if(this->_inventory[i] != NULL)
            delete (this->_inventory[i]);
    }
}

//Operators

const Character& Character::operator= (Character const &c)
{
	
	unsigned int i = 0;

	if(this != &c)
	{
		this->_name = c._name;
		while(i < NUM_OF_SLOTS)
		{

            if(this->_inventory[i] != NULL)
                delete (this->_inventory[i]);
			i++;
		}
		i = 0;
		while(i < NUM_OF_SLOTS && c._inventory[i] != NULL)
		{
			if(typeid(*(c._inventory[i])).name() == typeid(Ice).name())
			{
				std::cout<<"Assigning ice"<<std::endl;
				this->_inventory[i] = new Ice();
				*(this->_inventory[i]) = *(c._inventory[i]);
			}
			else if(typeid(*(c._inventory[i])).name() == typeid(Cure).name())
			{
				std::cout<<"Assigning cure"<<std::endl;
				this->_inventory[i] = new Cure();
				*(this->_inventory[i]) = *(c._inventory[i]);
			}
			else
			{
				std::cout<<"Non existing AMateria"<<std::endl;
			}
			i++;
		}
	}

	return *this;
}

//Virtual inherited methods
std::string const & Character::getName () const
{
    return (this->_name);
}

void Character::equip (AMateria *m)
{
    unsigned int i = 0;
   	
    while(i < NUM_OF_SLOTS && this->_inventory[i] != NULL)
			i++;
    if(i >= NUM_OF_SLOTS)
    {
        std::cout<<"All slots are ocuppied!!!"<<std::endl;
    }
	else if(m == NULL)
	{
	
        std::cout<<"Non existing AMateria"<<std::endl;
	}
	else if(typeid(*m).name() == typeid(Ice).name())
    {
        std::cout<<"Assigning ice"<<std::endl;
        this->_inventory[i] = new Ice();
        *(this->_inventory[i]) = *(m);
    }
    else if(typeid(*m).name() == typeid(Cure).name())
    {
        std::cout<<"Assigning cure"<<std::endl;
        this->_inventory[i] = new Cure();
        *(this->_inventory[i]) = *(m);
    }
}

void Character::unequip (int idx)
{
    if(idx >= NUM_OF_SLOTS || idx < 0)
    {
        std::cout<<"Index is not in range"<<std::endl;
    }
    else if(this->_inventory[idx] == NULL)
    {
        std::cout<<"This slot is already unused"<<std::endl;
    }
    else
    {
        addElementToDelete(this->_inventory[idx]);
        this->_inventory[idx] = NULL;
    }
}

void Character::use (int idx, ICharacter &target)
{ 
    if(idx >= NUM_OF_SLOTS)
    {
        std::cout<<"Index is not in range"<<std::endl;
    }
    else if(this->_inventory[idx] == NULL)
    {
        std::cout<<"This slot is already unused"<<std::endl;
    }
    else
    {
        this->_inventory[idx]->use(target);
    }
}

//Auxiliar functions
//Va reservando memoria todo el rato pero no actualiza el puntero
void Character::addElementToDelete (AMateria *materia)
{
	t_materias *aux;
	aux = new t_materias;
	aux->materia = materia;
	aux->next = delete_list;
	delete_list = aux;
}

void Character::removeDeleteList(void)
{
	t_materias *aux;

	while(delete_list != NULL)
	{
		delete (delete_list->materia);
		aux = delete_list;	
		delete_list = delete_list->next;
		delete (aux);
	}
}

void Character::copyDeleteList(t_materias *dl)
{

	t_materias *aux, *aux2;
	if(dl == NULL)
	{
		return ;
	}
	else
	{	
		aux = dl;
		delete_list = new t_materias;
		delete_list->next = NULL;
		aux2 = delete_list;
		while(aux != NULL)
		{
			if(typeid(*(aux->materia)).name() == typeid(Ice).name())
			{
				std::cout<<"Assigning ice"<<std::endl;
				aux2->materia = new Ice();
				*(aux2->materia) = *(aux->materia);
				aux2->next = NULL;
			}
			else if(typeid(*(aux->materia)).name() == typeid(Cure).name())
			{
				std::cout<<"Assigning cure"<<std::endl;
				aux->materia = new Cure();
				*(aux2->materia) = *(aux->materia);
				aux2->next = NULL;
			}
			aux = aux->next;
			if(aux != NULL)
			{
				aux2->next = new t_materias;
				aux2 = aux2->next;
			}	
		}
	}	
}
