
#include "../inc/Ice.hpp"

//Constructors and destructor
Ice::Ice (void)
	: AMateria("ice")
{
}

Ice::Ice (Ice const &ice)
	: AMateria(ice._type)
{}

Ice::~Ice()
{
	std::cout<<"Calling Ice destructor"<<std::endl;
}

//Operators

const Ice& Ice::operator= (Ice const &ice)
{
    AMateria::operator= (ice);

    return *this;
}

//Inherited virtual methods

AMateria* Ice::clone () const
{
	Ice *ice = new Ice();
	
	return ice;	
}

void Ice::use (ICharacter& target)
{
	std::cout<<"* shoots an ice bolt at "<<target.getName()<<" *"<<std::endl;
}
