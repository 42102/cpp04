
#include <iostream>
#include <stdlib.h>
#include "../inc/Character.hpp"
#include "../inc/IMateriaSource.hpp"
#include "../inc/MateriaSource.hpp"
#include "../inc/Ice.hpp"
#include "../inc/Cure.hpp"
#include "../inc/Character.hpp"

/*void check_leaks(void)
{
	system("leaks -q ./materia");
}
*/

int main (void)
{

	//atexit(check_leaks);
    
	Character c("Pepe");
    Character d("Juan");
    Character e(d);
	IMateriaSource *me = new MateriaSource();
	Ice i1;
    Cure i2;
    Cure i3;
    Ice i4;
    Cure i5;

	

	me->learnMateria(&i5);
	std::cout<<std::endl;

    c.equip(&i1);
    c.equip(&i2);
    c.equip(&i3);
    
	c.equip(me->createMateria("cure"));

    c.unequip(3);
    c.unequip(2);
	c.unequip(1);
	c.unequip(0);
	c.unequip(-5);


    c.equip(&i5);
    c.use(2,e);

	delete me;
    return 0;
}
