
#ifndef ICE_HPP_
#define ICE_HPP_

#include "AMateria.hpp"
#include <iostream>

class Ice: public AMateria
{
	public:
		//Constructors and destructor
		Ice(void);
		Ice(Ice const &ice);
		~Ice();
		
		//Operators
		const Ice& operator= (Ice const &ice);
		
		//Virtual inherited methods
		AMateria* clone() const;
		void use (ICharacter& target);
			
};

#endif
