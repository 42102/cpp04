
#ifndef CHARACTER_HPP_
#define CHARACTER_HPP_

#include "AMateria.hpp"
#include "Ice.hpp"
#include "Cure.hpp"
#include "ICharacter.hpp"
#include <typeinfo>
#include <iostream>

#define NUM_OF_SLOTS 4
typedef struct s_materias
{
	AMateria *materia;
	struct s_materias *next;
}t_materias;

class Character: public ICharacter
{
	private:
		std::string _name;
		AMateria *_inventory[NUM_OF_SLOTS];
		t_materias *delete_list;

		void addElementToDelete(AMateria *materia);
		void removeDeleteList(void);
		void copyDeleteList(t_materias *dl);
	public:
		//Constructors and destructor
		Character(void);
		Character(std::string name);
		Character(Character const &c);
		~Character();

		//Operators
		const Character& operator= (Character const &c);

		//Virtual inherited methods
		std::string const & getName() const;
		void equip(AMateria* m);
		void unequip(int idx);
		void use(int idx, ICharacter& target);
};

#endif
