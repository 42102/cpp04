
#ifndef AMATERIA_HPP_
#define AMATERIA_HPP_

class AMateria;
#include "ICharacter.hpp"
#include <iostream>

class AMateria
{
    protected:
        std::string _type;

    public:
        //Constructors and destructor
        AMateria(void);
        AMateria(AMateria const &materia);
        AMateria(std::string const & type);
        virtual ~AMateria();

        //Operators
        const AMateria& operator=(AMateria const &materia);

        //Methods
        std::string const & getType() const; //Returns the materia type
        virtual AMateria* clone() const = 0;
        virtual void use(ICharacter& target);
};

#endif
