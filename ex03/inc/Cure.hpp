
#ifndef CURE_HPP_
#define CURE_HPP_

#include "AMateria.hpp"
#include <iostream>

class Cure: public AMateria
{
	public:
		//Constructors and destructor
		Cure(void);
		Cure(Cure const &cure);
		~Cure();

		//Operators
		const Cure& operator= (Cure const &cure);

		//Virtual methods inherited
		AMateria* clone () const;
		void use (ICharacter &target);
};

#endif
