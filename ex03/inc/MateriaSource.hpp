
#ifndef MATERIA_SOURCE_HPP_
#define MATERIA_SOURCE_HPP_

#include "AMateria.hpp"
#include "Ice.hpp"
#include "Cure.hpp"
#include "IMateriaSource.hpp"

#include <iostream>

class MateriaSource: public IMateriaSource
{
    private:
        static const unsigned int NUM_MATERIAS = 4;
        AMateria *_materias[MateriaSource::NUM_MATERIAS];
    public:
        //Constructors and destructor
        MateriaSource(void);
        MateriaSource(MateriaSource const &materia_src);
        ~MateriaSource();

        //Operators
        const MateriaSource& operator= (MateriaSource const &materia_src);

        //Virtual inherited methods
        void learnMateria(AMateria*);
        AMateria* createMateria(std::string const & type);
 
};

#endif
