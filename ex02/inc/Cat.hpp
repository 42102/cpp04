
#ifndef CAT_HPP_
#define CAT_HPP_

#include "./AAnimal.hpp"
#include <iostream>

class Cat: public AAnimal
{
    public:
        //Constructors and destructors
        Cat(void);
        Cat(Cat const &kitten);
        ~Cat();

        //Operators
        const Cat& operator= (Cat const &kitten);

        //Methods
        void makeSound(void) const;
};

#endif
