
#ifndef AANIMAL_HPP_
#define AANIMAL_HPP_
#include <iostream>

class AAnimal{
    
    protected:
        std::string _type;

    public:
        //Constructors and Destructors
        AAnimal(void);
        AAnimal(std::string type);
        AAnimal(AAnimal const &a);
        virtual ~AAnimal();
        
        //Operators

        const AAnimal& operator= (AAnimal const &a);

        //Methods
        virtual void makeSound(void) const = 0;
};

#endif
