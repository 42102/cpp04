
#include "../inc/Dog.hpp"

//Constructors and destructors
Dog::Dog(void)
    : AAnimal("Dog")
{
    std::cout<<"Dog default constructor"<<std::endl;
}

Dog::Dog(Dog const &doggy)
    : AAnimal(doggy._type)
{
    std::cout<<"Dog copy constructor"<<std::endl;
}

Dog::~Dog()
{
    std::cout<<"Calling Dog destructor"<<std::endl;
}

//Operators
const Dog& Dog::operator= (Dog const &doggy)
{
    if(this != &doggy)
        AAnimal::operator=(doggy);

    return *this;
}

//Methods
void Dog::makeSound(void) const
{
    std::cout<<this->_type<<" does guau guau guau!!!"<<std::endl;
}
