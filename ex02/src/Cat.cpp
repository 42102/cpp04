
#include "../inc/Cat.hpp"

//Constructors and destructor

Cat::Cat (void)
    : AAnimal("Cat")
{
    std::cout<<"Cat default constructor"<<std::endl;
}

Cat::Cat (Cat const &kitten)
    : AAnimal(kitten._type)
{
    std::cout<<"Cat copy constructor"<<std::endl;
}

Cat::~Cat ()
{
    std::cout<<"Calling Cat destructor"<<std::endl;
}

//Operators

const Cat& Cat::operator= (Cat const &kitten)
{
    if(this != &kitten)
        AAnimal::operator= (kitten);
    
    return *this;
}

//Methods

void Cat::makeSound(void) const
{
    std::cout<<this->_type<<" makes miau miauu miauu!!!"<<std::endl;
}
