
#include "../inc/WrongCat.hpp"

//Constructors and destructor

WrongCat::WrongCat (void)
    : WrongAnimal("WrongCat")
{
    std::cout<<"WrongCat default constructor"<<std::endl;
}

WrongCat::WrongCat (WrongCat const &kitten)
    : WrongAnimal(kitten._type)
{
    std::cout<<"WrongCat copy constructor"<<std::endl;
}

WrongCat::~WrongCat ()
{
    std::cout<<"Calling WrongCat destructor"<<std::endl;
}

//Operators

const WrongCat& WrongCat::operator= (WrongCat const &kitten)
{
    if(this != &kitten)
        WrongAnimal::operator= (kitten);
    
    return *this;
}

//Methods

void WrongCat::makeSound(void) const
{
    std::cout<<this->_type<<" makes miau miauu miauu!!!"<<std::endl;
}
