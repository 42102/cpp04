
#include "../inc/Dog.hpp"
#include "../inc/AAnimal.hpp"
#include "../inc/Cat.hpp"

#include <iostream>

int main(void)
{
    AAnimal *a = new Dog();
    AAnimal *b = new Cat();
    
    a->makeSound();
    b->makeSound();

    delete a;
    delete b;

    return 0;
}
