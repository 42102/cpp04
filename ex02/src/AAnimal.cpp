
#include "../inc/AAnimal.hpp"

//Constructors and destructors

AAnimal::AAnimal(void)
    : _type("")
{
    std::cout<<"AAnimal default constructor"<<std::endl;
}

AAnimal::AAnimal(std::string type)
    :_type(type)
{
    std::cout<<"AAnimal constructor with 1 parameter<string>"<<std::endl;
}

AAnimal::AAnimal(AAnimal const &a)
    :_type(a._type)
{
    std::cout<<"AAnimal copy constructor"<<std::endl;
}

AAnimal::~AAnimal()
{
    std::cout<<"Calling AAnimal destructor"<<std::endl;
}

//Operators

const AAnimal& AAnimal::operator= (AAnimal const &a)
{
    if(this != &a)
        this->_type = a._type;
    
    return *this;
}

