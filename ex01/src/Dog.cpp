
#include "../inc/Dog.hpp"

//Constructors and destructors
Dog::Dog(void)
    : Animal("Dog")
{
    this->_brain = new Brain();
    std::cout<<"Dog default constructor"<<std::endl;
}

Dog::Dog(Dog const &doggy)
    : Animal(doggy._type)
{
    std::cout<<"Dog copy constructor"<<std::endl;
    this->_brain = new Brain();
    *(this->_brain) = *(doggy._brain);
}

Dog::~Dog()
{
    std::cout<<"Calling Dog destructor"<<std::endl;
    delete (this->_brain);
}

//Operators
const Dog& Dog::operator= (Dog const &doggy)
{
    if(this != &doggy)
    {
        Animal::operator=(doggy);
        *(this->_brain) = *(doggy._brain);
    }

    return *this;
}

//Methods
void Dog::makeSound(void) const
{
    std::cout<<this->_type<<" does guau guau guau!!!"<<std::endl;
}
