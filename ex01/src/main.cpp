
#include "../inc/Dog.hpp"
#include "../inc/Animal.hpp"
#include "../inc/Cat.hpp"
#include "../inc/WrongAnimal.hpp"
#include "../inc/WrongCat.hpp"

#include <iostream>


int main(void)
{
    const unsigned int number_of_animals = 100;

    Animal *animals[number_of_animals];

    for(unsigned int i = 0; i < number_of_animals; i++)
    {
        if(i <= (number_of_animals / 2))
            animals[i] = new Dog();
        else
            animals[i] = new Cat();
    }

    for(unsigned int i = 0; i < number_of_animals; i++)
    {
        delete animals[i];
    }

    return 0;
}
