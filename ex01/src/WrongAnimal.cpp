
#include "../inc/WrongAnimal.hpp"

//Constructors and destructors

WrongAnimal::WrongAnimal(void)
    : _type("")
{
    std::cout<<"WrongAnimal default constructor"<<std::endl;
}

WrongAnimal::WrongAnimal(std::string type)
    :_type(type)
{
    std::cout<<"WrongAnimal constructor with 1 parameter<string>"<<std::endl;
}

WrongAnimal::WrongAnimal(WrongAnimal const &a)
    :_type(a._type)
{
    std::cout<<"WrongAnimal copy constructor"<<std::endl;
}

WrongAnimal::~WrongAnimal()
{
    std::cout<<"Calling WrongAnimal destructor"<<std::endl;
}

//Operators

const WrongAnimal& WrongAnimal::operator= (WrongAnimal const &a)
{
    if(this != &a)
        this->_type = a._type;
    
    return *this;
}

//Methods

void WrongAnimal::makeSound(void) const
{
    std::cout<<"Wrong WrongAnimal method make sound"<<std::endl;
}
