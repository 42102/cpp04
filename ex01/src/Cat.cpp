
#include "../inc/Cat.hpp"

//Constructors and destructor

Cat::Cat (void)
    : Animal("Cat")
{
    this->_brain = new Brain();
    std::cout<<"Cat default constructor"<<std::endl;
}

Cat::Cat (Cat const &kitten)
    : Animal(kitten._type)
{
    std::cout<<"Cat copy constructor"<<std::endl;
    this->_brain = new Brain();
    *(this->_brain) = *(kitten._brain);
}

Cat::~Cat ()
{
    std::cout<<"Calling Cat destructor"<<std::endl;
    delete (this->_brain);
}

//Operators

const Cat& Cat::operator= (Cat const &kitten)
{
    if(this != &kitten)
    {
        *(this->_brain) = *(kitten._brain);
        Animal::operator= (kitten);
    }
    return *this;
}

//Methods

void Cat::makeSound(void) const
{
    std::cout<<this->_type<<" makes miau miauu miauu!!!"<<std::endl;
}
