
#include "../inc/Brain.hpp"

//Constructors and destructor

Brain::Brain (void)
{
    std::cout<<"Brain default constructor"<<std::endl;
}

Brain::Brain (Brain const &b)
{
    std::cout<<"Brain copy constructor"<<std::endl;
    *this =  b;
}

Brain::~Brain()
{
    std::cout<<"Calling Brain destructor"<<std::endl;
}

//Operators

const Brain& Brain::operator= (Brain const &b)
{
    if(this != &b)
    { 
        for(unsigned int i = 0; i < number_of_ideas; i++)
        {
            this->ideas[i] = b.ideas[i];
        }
    }

    return *this;
}
