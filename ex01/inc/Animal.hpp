
#ifndef ANIMAL_HPP_
#define ANIMAL_HPP_
#include <iostream>

class Animal{
    
    protected:
        std::string _type;

    public:
        //Constructors and Destructors
        Animal(void);
        Animal(std::string type);
        Animal(Animal const &a);
        virtual ~Animal();
        
        //Operators

        const Animal& operator= (Animal const &a);

        //Methods
        virtual void makeSound(void) const;
};

#endif
