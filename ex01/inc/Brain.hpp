
#ifndef BRAIN_HPP_
#define BRAIN_HPP_

#include <iostream>

class Brain
{
    private:
        static const unsigned int number_of_ideas = 100;

    public:
        //Variables
        std::string ideas[number_of_ideas];

        //Constructors and destructor
        Brain(void);
        Brain(Brain const &b);
        ~Brain();

        //Operator
        const Brain& operator= (Brain const &b);
};
#endif
