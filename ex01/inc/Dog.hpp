
#ifndef DOG_HPP_
#define DOG_HPP_

#include "./Animal.hpp"
#include "Brain.hpp"

#include <iostream>

class Dog: public Animal
{
    private:
        Brain *_brain;

    public:
        //Constructors and destructors
        Dog(void);
        Dog(Dog const &doggy);
        ~Dog();

        //Operators
        const Dog& operator= (Dog const &doggy);

        //Methods
        void makeSound(void) const;

};

#endif
