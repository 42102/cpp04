
#ifndef WRONG_CAT_HPP_
#define WRONG_CAT_HPP_

#include "./WrongAnimal.hpp"
#include <iostream>

class WrongCat: public WrongAnimal
{
    public:
        //Constructors and destructors
        WrongCat(void);
        WrongCat(WrongCat const &kitten);
        ~WrongCat();

        //Operators
        const WrongCat& operator= (WrongCat const &kitten);

        //Methods
        void makeSound(void) const;
};

#endif
