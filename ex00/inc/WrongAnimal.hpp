
#ifndef WRONG_ANIMAL_HPP_
#define WRONG_ANIMAL_HPP_
#include <iostream>

class WrongAnimal{
    
    protected:
        std::string _type;

    public:
        //Constructors and Destructors
        WrongAnimal(void);
        WrongAnimal(std::string type);
        WrongAnimal(WrongAnimal const &a);
        ~WrongAnimal();
        
        //Operators

        const WrongAnimal& operator= (WrongAnimal const &a);

        //Methods
        void makeSound(void) const;
};

#endif
