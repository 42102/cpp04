
#ifndef CAT_HPP_
#define CAT_HPP_

#include "./Animal.hpp"
#include <iostream>

class Cat: public Animal
{
    public:
        //Constructors and destructors
        Cat(void);
        Cat(Cat const &kitten);
        ~Cat();

        //Operators
        const Cat& operator= (Cat const &kitten);

        //Methods
        void makeSound(void) const;
};

#endif
