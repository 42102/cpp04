
#ifndef DOG_HPP_
#define DOG_HPP_

#include "./Animal.hpp"
#include <iostream>

class Dog: public Animal
{
    public:
        //Constructors and destructors
        Dog(void);
        Dog(Dog const &doggy);
        ~Dog();

        //Operators
        const Dog& operator= (Dog const &doggy);

        //Methods
        void makeSound(void) const;

};

#endif
