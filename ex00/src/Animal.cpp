
#include "../inc/Animal.hpp"

//Constructors and destructors

Animal::Animal(void)
    : _type("")
{
    std::cout<<"Animal default constructor"<<std::endl;
}

Animal::Animal(std::string type)
    :_type(type)
{
    std::cout<<"Animal constructor with 1 parameter<string>"<<std::endl;
}

Animal::Animal(Animal const &a)
    :_type(a._type)
{
    std::cout<<"Animal copy constructor"<<std::endl;
}

Animal::~Animal()
{
    std::cout<<"Calling animal destructor"<<std::endl;
}

//Operators

const Animal& Animal::operator= (Animal const &a)
{
    if(this != &a)
        this->_type = a._type;
    
    return *this;
}

//Methods
void Animal::makeSound(void) const
{
    std::cout<<"Sound of undefined animal"<<std::endl;
}
