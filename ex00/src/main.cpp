
#include "../inc/Dog.hpp"
#include "../inc/Animal.hpp"
#include "../inc/Cat.hpp"
#include "../inc/WrongAnimal.hpp"
#include "../inc/WrongCat.hpp"

#include <iostream>

int main(void)
{
    Animal *a = new Dog();
    Animal *b = new Cat();
    WrongAnimal *c = new WrongCat();
    
    a->makeSound();
    b->makeSound();
    c->makeSound();

    delete a;
    delete b;
    delete c;

    return 0;
}
